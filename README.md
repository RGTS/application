![picture](https://i.imgur.com/ulU6Tmh.png)

Unofficial Twitch client, Open source and multi-platform for all platforms to use.

## Version 1.0.2-4

Watch and be part of the action just like on the web version all wrapped up into a desktop application!

![picture](https://i.imgur.com/R2YC5Lg.png)

![picture](https://i.imgur.com/ceforCm.png)

Even manage and run your streams conveniently from the Twitch application.

![picture](https://i.imgur.com/M0OVHOn.png)


 ## Linux 64bit

 You can install twitch from the AUR for Arch/Manjaro distros.
 [Click Here](https://aur.archlinux.org/packages/twitch/)

 ### Zipped Portable File:

 ### Linux x64
 - [Download](https://gitlab.com/twitch-application/binaries/-/raw/master/Twitch-linux-x64.tar.gz)

 ### Linux Arm64 Beta
 - [Download](https://gitlab.com/twitch-application/binaries/-/raw/master/Twitch-linux-arm64.tar.gz)

 ### Linux Arm7l Beta
 - [Download](https://gitlab.com/twitch-application/binaries/-/raw/master/Twitch-linux-arm64.tar.gz)

 ## Windows 64bit

 ### Windows Setup:
 - [Download](https://gitlab.com/twitch-application/binaries/-/raw/master/Twitch%20Setup.exe)

 ### Zipped Portable File:
 - [Download](https://gitlab.com/twitch-application/binaries/-/raw/master/Twitch-win32-x64.zip)

 ## Mac OS 64bit

 ###  Zipped Portable File:
 - [Download](https://gitlab.com/twitch-application/binaries/-/raw/master/Twitch-darwin-x64.zip)



 ### Author
  * Corey Bruce
